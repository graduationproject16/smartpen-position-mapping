// position mapping.cpp : Defines the entry point for the console application.

#include "stdafx.h"
#include "wtypes.h"
#include <windows.h>
#include <cstdlib>
#include <conio.h>
#include <iostream>

using namespace std;

int main()
{
	//The position of the pen in respect to the paper.
	double spaceX = 7.562; // spaceX in centimeter 
	double spaceY = 5.727; // spaceY in centimeter

	//The position of the cursor on the screen.
	int pixelsX = (spaceX * 100 * 960) / 25.4; // spaceX in pixels 
	int pixelsY = (spaceY * 100 * 960) / 25.4; // spaceY in pixels


	cout << "The coordinates of the pen is X = " << spaceX << " - Y = " << spaceY << endl;
	SetCursorPos(spaceX, spaceY);


	POINT mouse_position; GetCursorPos(&mouse_position);


	return 0;

}



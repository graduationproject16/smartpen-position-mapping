# Smart Pen
Smartpen is a digital note-taking system that enables the user to take notes on regular paper
and see their notes transferred to their computer device where they can share using websites system in real time.

# This represents the mapping from pen position on the paper to the screen.
* A desktop application that receives the coordinates of the pen -
in centimeters - , convert it into pixels according to the screen
resolution and control the cursor to move it.

* Step 3 of the project. This application receives the values from [step 2](https://bitbucket.org/graduationproject16/smartpen-pen-posision).

* Created by Mohamed Mahmoud Kamel.

To check the project full documentation visit this [link](https://drive.google.com/file/d/1U_OQTZ40pwyhT3x178S9Y90uItywCBWm/view?usp=sharing)